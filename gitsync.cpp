#include <iostream>
#include <string>
#include <git2.h>
#include <sys/stat.h>
#include <filesystem>

using namespace std;

static const char *get_branch(git_repository *repo)
{
  int error = 0;
  const char *branch = "";
  git_reference *head = NULL;

  error = git_repository_head(&head, repo);

  if (error == GIT_EUNBORNBRANCH || error == GIT_ENOTFOUND)
    branch = "";
  else if (!error)
  {
    branch = git_reference_shorthand(head);
  }

  git_reference_free(head);

  return branch;
}

bool gitExists(string dir)
{
  struct stat st;
  if (stat((dir + ".git").c_str(), &st) == 0)
    return true;
  return false;
}

int main()
{
  git_libgit2_init();

  string REPO_PATH = "./";

  if (!gitExists(REPO_PATH))
  {
    for (int i = 10; i > 0; i--)
    {
      REPO_PATH += "../";
      if (gitExists(REPO_PATH))
        break;
    }
    if (!gitExists(REPO_PATH)) {
      cout << "Could not find a git repository." << endl;
      return -1;
    }
  }

  git_repository *repo = nullptr;
  git_repository_open(&repo, REPO_PATH.c_str());

  const string branch = get_branch(repo);

  if (branch == "")
  {
    cout << "Branch not found" << endl;
    return -1;
  }

  cout << "branch: " << branch << endl;

  git_strarray remotes;

  git_remote_list(&remotes, repo);

  for (int i = 0; i < remotes.count; i++)
  {
    const string remote = remotes.strings[i];
    cout << "remote: " << remote << endl;
    system(("git pull " + remote + " " + branch).c_str());
    system(("git push " + remote + " " + branch).c_str());
    // TODO: implement in git2
    /*git_remote *remote = {0};
    git_remote_lookup(&remote, repo, remotes.strings[i]);
    cout << "fetching " << remotes.strings[i] << endl;
    git_remote_fetch(remote, NULL, NULL, NULL);
    cout << "pushing " << remotes.strings[i] << endl;
    git_remote_push(remote, NULL, NULL);*/
  }

  return 0;
}