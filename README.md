# Gitsync

A very simple program to sync your working branch with all remotes.

## Usage

```bash
git clone https://gitlab.com/wcyat/gitsync.git
cd gitsync
make
make install
```

```bash
gitsync
```
